const serviceTab = document.querySelectorAll(".service-tab-item");
const serviceContent = document.querySelectorAll(".service-img-item");
const graphicDesignImgs = document.querySelectorAll(".graphic-design");
const webDesignImgs = document.querySelectorAll(".web-design");
const landingPagesImgs = document.querySelectorAll(".landing-pages");
const wordpressImgs = document.querySelectorAll(".wordpress");
const grid = document.querySelector(".grid");
const workImgContainer = document.querySelector(".work-img-container");
const imagesItem = document.querySelectorAll(".work-img-container img");
const loadMoreBtn = document.querySelector("#btn1");
const loadMoreBtnBottom = document.querySelector("#btn2");
const loadAnim = document.querySelector(".loadAnim");
const loadAnim2 = document.querySelector(".loadAnim2");
const allButtons = document.querySelectorAll(".work-tab-item");
const feedbackItems = document.querySelectorAll(".client-feedback-item");
const feedbackTabs = document.querySelectorAll(".client-feedback-tab-foto");
const prevBtn = document.querySelector("#prev");
const nextBtn = document.querySelector("#next");

const loader = document.createElement("div");
loader.classList.add("water");
let currentIndex = 0;
let count = 0;
let imageCount = 12;

serviceTab.forEach((tab, index) => {
  tab.addEventListener("click", () => {
    serviceTab.forEach((tab) => tab.classList.remove("active"));
    tab.classList.add("active");

    serviceContent.forEach((content) => content.classList.remove("active"));
    serviceContent[index].classList.add("active");
  });
});

feedbackTabs.forEach((tab, index) => {
  tab.addEventListener("click", () => {
    currentIndex = index;
    updateFeedback(currentIndex);
  });
});

prevBtn.addEventListener("click", () => {
  currentIndex--;
  if (currentIndex < 0) {
    currentIndex = feedbackItems.length - 1;
  }
  updateFeedback(currentIndex);
});

nextBtn.addEventListener("click", () => {
  currentIndex++;
  if (currentIndex > feedbackItems.length - 1) {
    currentIndex = 0;
  }
  updateFeedback(currentIndex);
});

function showAllImgs() {
  const allImgs = document.querySelectorAll(".work-img-container img");
  allImgs.forEach((img) => {
    img.style.display = "flex";
  });
}

function hideAllImgs() {
  const allImgs = document.querySelectorAll(".work-img-container img");
  allImgs.forEach((img) => {
    img.style.display = "none";
  });
}

function showImgs(imgs) {
  imgs.forEach((img) => {
    img.style.display = "flex";
  });
}

function updateFeedback(currentIndex) {
  feedbackItems.forEach((item) => {
    item.classList.remove("active");
  });
  feedbackTabs.forEach((tab) => {
    tab.classList.remove("active");
  });
  feedbackItems[currentIndex].classList.add("active");
  feedbackTabs[currentIndex].classList.add("active");
}

allButtons.forEach((tab) => {
  tab.addEventListener("click", function () {
    allButtons.forEach((tab) => tab.classList.remove("active"));
    tab.classList.add("active");
    const category = this.textContent.toLowerCase();
    const images = document.querySelectorAll(".work-img-container img");
    images.forEach((image) => {
      if (category === "all" || image.dataset.category === category) {
        image.style.display = "block";
      } else {
        image.style.display = "none";
      }
    });
  });
});

const images = [
  {
    category: "Graphic Design",
    url: "/img/graphic design/graphic-design1.jpg",
  },
  {
    category: "Graphic Design",
    url: "/img/graphic design/graphic-design2.jpg",
  },
  {
    category: "Graphic Design",
    url: "/img/graphic design/graphic-design3.jpg",
  },
  {
    category: "Graphic Design",
    url: "/img/graphic design/graphic-design4.jpg",
  },
  {
    category: "Graphic Design",
    url: "/img/graphic design/graphic-design5.jpg",
  },
  {
    category: "Graphic Design",
    url: "/img/graphic design/graphic-design6.jpg",
  },
  {
    category: "Graphic Design",
    url: "/img/graphic design/graphic-design7.jpg",
  },
  {
    category: "Graphic Design",
    url: "/img/graphic design/graphic-design8.jpg",
  },
  {
    category: "Graphic Design",
    url: "/img/graphic design/graphic-design9.jpg",
  },
  {
    category: "Graphic Design",
    url: "/img/graphic design/graphic-design10.jpg",
  },
  {
    category: "Graphic Design",
    url: "/img/graphic design/graphic-design11.jpg",
  },
  {
    category: "Graphic Design",
    url: "/img/graphic design/graphic-design12.jpg",
  },
  { category: "Web Design", url: "/img/web design/web-design1.jpg" },
  { category: "Web Design", url: "/img/web design/web-design2.jpg" },
  { category: "Web Design", url: "/img/web design/web-design3.jpg" },
  { category: "Web Design", url: "/img/web design/web-design4.jpg" },
  { category: "Web Design", url: "/img/web design/web-design5.jpg" },
  { category: "Web Design", url: "/img/web design/web-design6.jpg" },
  { category: "Web Design", url: "/img/web design/web-design7.jpg" },
  { category: "Landing Pages", url: "/img/landing page/landing-page1.jpg" },
  { category: "Landing Pages", url: "/img/landing page/landing-page2.jpg" },
  { category: "Landing Pages", url: "/img/landing page/landing-page3.jpg" },
  { category: "Landing Pages", url: "/img/landing page/landing-page4.jpg" },
  { category: "Landing Pages", url: "/img/landing page/landing-page5.jpg" },
  { category: "Landing Pages", url: "/img/landing page/landing-page6.jpg" },
  { category: "Landing Pages", url: "/img/landing page/landing-page7.jpg" },
  { category: "Wordpress", url: "/img/wordpress/wordpress1.jpg" },
  { category: "Wordpress", url: "/img/wordpress/wordpress2.jpg" },
  { category: "Wordpress", url: "/img/wordpress/wordpress3.jpg" },
  { category: "Wordpress", url: "/img/wordpress/wordpress4.jpg" },
  { category: "Wordpress", url: "/img/wordpress/wordpress5.jpg" },
  { category: "Wordpress", url: "/img/wordpress/wordpress6.jpg" },
  { category: "Wordpress", url: "/img/wordpress/wordpress7.jpg" },
  { category: "Wordpress", url: "/img/wordpress/wordpress8.jpg" },
  { category: "Wordpress", url: "/img/wordpress/wordpress9.jpg" },
  { category: "Wordpress", url: "/img/wordpress/wordpress10.jpg" },
  { category: "Wordpress", url: "/img/wordpress/wordpress11.jpg" },
  { category: "Wordpress", url: "/img/wordpress/wordpress12.jpg" },
];

function loadImages(images) {
  images.forEach((image) => {
    const img = document.createElement("img");
    img.setAttribute("src", image.url);
    img.setAttribute("data-category", image.category.toLowerCase());
    img.setAttribute("width", "285px");
    img.setAttribute("height", "206px");
    img.classList.add(image.category.toLowerCase().replace(" ", "-"));
    workImgContainer.appendChild(img);
  });
}

loadMoreBtn.addEventListener("click", () => {
  loadMoreBtn.style.display = "none";
  loadAnim.appendChild(loader);

  setTimeout(() => {
    const imagesToShow = images.slice(imageCount, imageCount + 12); // вибрати наступні 12 картинок для завантаження
    loadImages(imagesToShow);
    imageCount += 12;
    loadAnim.removeChild(loader);
    loadMoreBtn.style.display = "flex";
    if (imageCount >= 36) {
      loadMoreBtn.style.display = "none";
    }
  }, 2000);
});

$(document).ready(function () {
  const $grid = $(".grid").masonry({
    itemSelector: ".grid-item",
    columnWidth: ".grid-sizer",
    percentPosition: true,
  });

  $grid.imagesLoaded().progress(function () {
    $grid.masonry("layout");
  });

  $("#btn2").on("click", function () {
    loadMoreBtnBottom.style.display = "none";
    loadAnim2.appendChild(loader);

    setTimeout(() => {
      const imagesToShow = images.slice(count, count + 1);

      imagesToShow.forEach((image) => {
        const $img = $("<img>", { src: image.url });
        const $gridItem = $('<div class="grid-item">')
          .append($img)
          .appendTo($grid);
        $grid.masonry("appended", $gridItem).masonry("layout");
      });
      count += 1;
      loadAnim2.removeChild(loader);
      loadMoreBtnBottom.style.display = "flex";
      if (count >= images.length) {
        loadMoreBtnBottom.style.display = "none";
      }
    }, 1000);
  });
});
